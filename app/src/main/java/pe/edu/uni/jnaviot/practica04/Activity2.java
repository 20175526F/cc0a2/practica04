package pe.edu.uni.jnaviot.practica04;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class Activity2 extends AppCompatActivity {

    TextView text_view_plate_name;
    Spinner spinner_logo1;
    ArrayAdapter<CharSequence> adapter;
    LinearLayout linear_layout2;

    EditText et_name, et_address;
    Button b_enviar;
    RadioButton rb_visa, rb_efectivo;

    String name, address,numberplates;
    Boolean visa, efectivo;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        text_view_plate_name = findViewById(R.id.a2_text_view_plato);
        spinner_logo1 = findViewById(R.id.spinner_1);
        et_name = findViewById(R.id.edit_text_name);
        et_address = findViewById(R.id.edit_text_address);
        b_enviar = findViewById(R.id.a2_button);
        rb_visa = findViewById(R.id.rb_visa);
        rb_efectivo = findViewById(R.id.rb_efectivo);
        linear_layout2 = findViewById(R.id.linear_layout2);

        adapter = ArrayAdapter.createFromResource(this, R.array.nro_platos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_logo1.setAdapter(adapter);

        Intent intent = getIntent();

        String mensaje = intent.getStringExtra("TEXT");
        text_view_plate_name.setText(mensaje);

        retrieveData();

        b_enviar.setOnClickListener(view -> {
            Resources res = getResources() ;
            if(et_name.getText().toString().equals("") ||et_address.getText().toString().equals("") || ! (rb_visa.isChecked() || rb_efectivo.isChecked() )){
                //Snackbar.make(linear_layout2, R.string.msg_snack_bar,Snackbar.LENGTH_INDEFINITE).setAction(R.string.msg_button_snack_bar, view1 -> {
                //}).show());
                Snackbar.make(linear_layout2, R.string.msg_snack_bar,Snackbar.LENGTH_INDEFINITE).setAction(R.string.msg_button_snack_bar, view1 -> {}).show();
            }
            else{

            }
        });

    }



    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData(){
        sharedPreferences=getSharedPreferences("saveData", Context.MODE_PRIVATE);
        name=et_name.getText().toString();
        address=et_address.getText().toString();
        visa=rb_visa.isChecked();
        efectivo = rb_efectivo.isChecked();
        numberplates = spinner_logo1.getSelectedItem().toString();

        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("keyname",name);
        editor.putString("keyaddress",address);
        editor.putBoolean("keypago1",visa);
        editor.putBoolean("keypago2",efectivo);
        editor.putString("keynplates",numberplates);
        editor.apply();

        Toast.makeText(this,R.string.text_save,Toast.LENGTH_SHORT).show();
    }

    private void retrieveData(){
        sharedPreferences=getSharedPreferences("saveData",Context.MODE_PRIVATE);
        name=sharedPreferences.getString("keyname",null);
        address=sharedPreferences.getString("keyaddress",null);
        visa=sharedPreferences.getBoolean("keypago1",false);
        efectivo=sharedPreferences.getBoolean("keypago2",false);
        numberplates = sharedPreferences.getString("keynplates",null);

        et_name.setText(name);
        et_address.setText(address);
        rb_visa.setChecked(visa);
        rb_efectivo.setChecked(efectivo);

    }

}